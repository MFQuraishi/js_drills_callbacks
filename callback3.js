const cards = require("./data/cards.json");

function getCards(listName, cb) {
  if (typeof listName !== "string" || typeof cb !== "function") {
    throw new Error("invalid arguments passed");
  }

  let cardList = cards[listName];

  setTimeout(
    () => (typeof cardList !== "undefined" ? cb(cardList) : cb("not found")),
    2 * 1000
  );
}

module.exports = {
  getCards,
};
