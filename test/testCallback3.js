const {getCards} = require("./../callback3");
const lists = require("./../data/lists.json");

let board = Object.keys(lists)[0];
let listID = lists[board][0]["id"];

let cb = (data) => console.log(data);

try{
    getCards(listID, cb);
} catch (error) {
    console.log(error);
}