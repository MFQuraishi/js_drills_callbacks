const {getInfos} = require("./../callback5");
const boards = require("./../data/boards.json");

let thanosBoardId = boards.filter(element => element["name"] === "Thanos")[0]["id"];
let cardsToFind = ["Mind", "Space"];

try {
    getInfos(thanosBoardId, cardsToFind);
} catch (error) {
    console.log(error);
}