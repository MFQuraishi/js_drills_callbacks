const {getBoardData} = require("./../callback1");
const board = require("./../data/boards.json");

let boardName = board[0]["id"];

let cb = (data) => console.log(data);

try{
    getBoardData(boardName, cb);
} catch (error) {
    console.log(error);
}