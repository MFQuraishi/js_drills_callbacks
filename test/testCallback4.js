const {getInfo} = require("./../callback4");
const boards = require("./../data/boards.json");

let thanosBoardId = boards.filter(element => element["name"] === "Thanos")[0]["id"];
let cardToFind = "Mind";

try {
    getInfo(thanosBoardId, cardToFind);
} catch (error) {
    console.log(error);
}