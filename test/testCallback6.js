const {getAllCards} = require("../callback6");
const boards = require("../data/boards.json");

let thanosBoardId = boards.filter(element => element["name"] === "Thanos")[0]["id"];

try {
    getAllCards(thanosBoardId);
} catch (error) {
    console.log(error);
}