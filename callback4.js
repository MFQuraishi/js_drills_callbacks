const { getBoardData } = require("./callback1");
const { getLists } = require("./callback2");
const { getCards } = require("./callback3");


function getInfo(thanosBoardId, cardToFind){

    if(typeof thanosBoardId !== "string" || typeof cardToFind !== "string"){
        throw new Error("invalid arguments passed!");
    }

    getBoardData(thanosBoardId, (boardData) => {
        console.log(boardData);
        getLists(boardData["id"], (lists) => {
            console.log(lists);
            let card = lists.find(element => element["name"] === cardToFind);
            getCards(card["id"], (cards) => {
                console.log(cards);
            });
        });
    });
}

module.exports = {
    getInfo
}