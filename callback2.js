const lists = require("./data/lists.json");

function getLists(boardName, cb){
    if (typeof boardName !== 'string' || typeof cb !== 'function'){
        throw new Error("invalid arguments passed");
    }

    

    setTimeout(() => {
        if(typeof lists[boardName] === "undefined"){
            cb("data not found");
        }else{
            cb(lists[boardName]);
        }
    } ,2 * 1000);
}

module.exports = {
    getLists
}