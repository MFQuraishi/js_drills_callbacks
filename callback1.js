const boards = require("./data/boards.json");

function getBoardData (boardName ,cb){
    if (typeof boardName !== 'string' || typeof cb !== 'function'){
        throw new Error("invalid arguments passed");
    }
    setTimeout(() => {
        let info = boards.find((boardElement) => boardElement["id"] === boardName);
        
        if (typeof info === "undefined"){
            cb("data not found");
        }
        else{
            cb(info)
        }
        
    } , 2 * 1000);
}

module.exports = { getBoardData }