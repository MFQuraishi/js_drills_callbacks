const { getBoardData } = require("./callback1");
const { getLists } = require("./callback2");
const { getCards } = require("./callback3");


function getAllCards(thanosBoardId){

    if(typeof thanosBoardId !== "string"){
        throw new Error("invalid arguments passed!");
    }

    getBoardData(thanosBoardId, (boardData) => {
        console.log(boardData);
        getLists(boardData["id"], (lists) => {
            console.log(lists);
            lists.forEach(listElement => {
                getCards(listElement["id"], (cards) => {
                    console.log(cards);
                });
            });
        });
    });
}


module.exports = {
    getAllCards
}

