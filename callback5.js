const { getBoardData } = require("./callback1");
const { getLists } = require("./callback2");
const { getCards } = require("./callback3");


function getInfos(thanosBoardId, cardsToFind){

    if(typeof thanosBoardId !== "string" || !Array.isArray(cardsToFind)){
        throw new Error("invalid arguments passed!");
    }

    getBoardData(thanosBoardId, (boardData) => {
        console.log(boardData);
        getLists(boardData["id"], (lists) => {
            console.log(lists);
            cardsToFind.forEach(cardToFind => {
                let card = lists.find(listElement => listElement["name"] === cardToFind);
                getCards(card["id"], (cards) => {
                    console.log(cards);
                });
            });
        });
    });
}

module.exports = {
    getInfos
}